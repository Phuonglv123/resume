import type { Config } from '@/types/data';
import { enUS } from 'date-fns/locale';
import type { ReadonlyDeep } from 'type-fest';

const config = {
  i18n: {
    locale: enUS,
    dateFormat: 'MMMM yyyy',
    translations: {
      now: 'now',
    },
  },
  meta: {
    title: 'Le Van Phuong - Frontend Developer',
    description:
      'I am an experienced Frontend developer with over 3 years of experience in designing and developing user-friendly web applications. My passion for technology and creativity drives me to come up with intuitive and innovative web solutions. I am a fast learner and always eager to keep up with the latest web development trends.',
    faviconPath: '/src/assets/me.jpeg',
  },
  pdf: {
    footer: 'This resume was generated using a custom-built static site generator. For more information, visit',
  },
} as const satisfies ReadonlyDeep<Config>;

export default config;
