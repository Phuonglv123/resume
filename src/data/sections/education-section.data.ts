import type { EducationSection } from '@/types/sections/education-section.types';
import type { ReadonlyDeep } from 'type-fest';
import { website } from '../helpers/links';

const educationSectionData = {
  config: {
    title: 'Education',
    slug: 'education',
    icon: 'fa6-solid:graduation-cap',
    visible: true,
  },
  diplomas: [
    {
      title: 'FPT Greenwich University',
      institution: 'Software engineering',
      image: import('@/assets/logos/fpt.jpeg'),
      dates: [new Date('2014.3'), new Date('2018.07')],
      description: '',
      links: [website({ url: '#' })],
    },
    {
      title: 'Cybersoft Academy',
      institution: 'Front-end Developer',
      image: import('@/assets/logos/cyber.jpeg'),
      dates: [new Date('2018.5'), new Date('2018.09')],
      description: '',
      links: [website({ url: '#' })],
    },
    {
      title: 'Cybersoft Academy',
      institution: 'FullStack Developer',
      image: import('@/assets/logos/cyber.jpeg'),
      dates: [new Date('2019.02'), new Date('2019.07')],
      description: '',
      links: [website({ url: '#' })],
    },
  ],
} as const satisfies ReadonlyDeep<EducationSection>;

export default educationSectionData;
