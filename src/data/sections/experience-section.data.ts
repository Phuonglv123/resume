import type { ExperienceSection } from '@/types/sections/experience-section.types';
import type { ReadonlyDeep } from 'type-fest';
import { facebook, github, instagram, linkedin, twitter, website } from '../helpers/links';
import {
  antd,
  bootstrap,
  chakraUi,
  css3,
  eslint,
  firebase,
  git,
  html5,
  javascript,
  jira,
  nextJs,
  nuxtJS,
  nx,
  pnpm,
  react,
  reactQuery,
  strapi,
  tailwindCss,
  typescript,
  vue,
  yarn,
} from '../helpers/skills';
import { es, ht } from 'date-fns/locale';

const experienceSectionData = {
  config: {
    title: 'Work experience',
    slug: 'experience',
    icon: 'fa6-solid:suitcase',
    visible: true,
  },
  jobs: [
    {
      role: 'Front End Developer',
      company: 'Galaxy Education JSC',
      image: import('@/assets/logos/galaxy.png'),
      dates: [new Date('2021-04'), null],
      description: `
        - Collaborated with UI/UX designers to create user-friendly web applications.
        - Developed responsive web pages and cross-browser compatible web applications using modern web development technologies.
        - Optimized website performance through code optimization, image compression, browser caching, and using performance evaluation tools. Worked with Backend to address problem-solving scenarios.
        - Integrated APIs with Microservice architecture.Worked with Product to develop educational products.
        - Built user identification systems, developed operational tools, built payment websites, managed payments, bills, and built web traffic attracting sites for students.
      `,
      tagsList: {
        title: 'Technologies',
        tags: [
          react(),
          nextJs(),
          typescript(),
          javascript(),
          vue(),
          tailwindCss(),
          pnpm(),
          yarn(),
          eslint(),
          nuxtJS(),
          bootstrap(),
          antd(),
          html5(),
          css3(),
          git(),
          jira(),
          strapi(),
        ],
      },
      links: [facebook({ url: '#' }), linkedin({ url: '#' })],
    },
    {
      role: 'Front End Developer',
      company: 'VNNIS CO,LTD',
      image: import('@/assets/logos/vienis.png'),
      dates: [new Date('2019-04'), new Date('2021-03')],
      description: `
        - WireframeMindset, analysis, and problem-solving.
        - Created HTML dynamic elements using Javascript.
        - Designed user dashboards with UX/UI Object, Prototype, JavaScript, ReactJS.
        - Worked with RESTful web service.
        - Knowledge of cloud products for business by Google and MicrosoftTested and debugged company products.
      `,
      tagsList: {
        title: 'Technologies',
        tags: [react(), vue(), eslint(), nuxtJS(), bootstrap(), nextJs(), pnpm(), antd()],
      },
      links: [website({ url: '#' }), instagram({ url: '#' })],
    },
  ],
} as const satisfies ReadonlyDeep<ExperienceSection>;

export default experienceSectionData;
