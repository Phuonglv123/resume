import type { PortfolioSection } from '@/types/sections/portfolio-section.types';
import type { ReadonlyDeep } from 'type-fest';
import { demo, github, mockups, website } from '../helpers/links';
import {
  antd,
  chakraUi,
  css3,
  docker,
  eslint,
  express,
  expressGateway,
  firebase,
  git,
  html5,
  jest,
  mongoDb,
  nestJs,
  nextJs,
  nodejs,
  nx,
  pnpm,
  postgreSql,
  prettier,
  react,
  sass,
  tailwindCss,
  typescript,
} from '../helpers/skills';

const portfolioSectionData = {
  config: {
    title: 'Projects',
    slug: 'projects',
    icon: 'fa6-solid:rocket',
    visible: true,
    screenshots: {
      title: 'Screenshots',
      icon: 'fa6-solid:images',
    },
  },
  projects: [
    {
      name: 'Personal Project',
      image: import('@/assets/portfolio/project-4.jpeg'),
      dates: [new Date('2016-05'), new Date('2018-07')],
      details: [
        { label: 'Team size', value: '1 people' },
        { label: 'My role', value: 'Full-Stack Developer' },
        { label: 'Company', value: '' },
        { label: 'Category', value: ['Web app', 'Product'] },
      ],
      pdfDetails: [
        { label: 'Demo', value: 'https://disco-ninjas-g321ol.netlify.app', url: '#' },
        { label: 'Repository', value: 'https://github.com/mark-freeman/disco-ninjas', url: '#' },
      ],
      description: `This is a project that I developed myself to improve my skills towards FullStack developer. This project I designed according to microservice architecture uses Express Gate to connect to services. For the backend of the project I used nodejs/ExpressJS and mongodb. For the Frontend I used React/Antd Pro.
      *  Project features include:
      - Core account management (user identifier)
      - Core manages catalogs
      - Core billing
      - Core payments
      - Automatically complete payment when user banktransfer`,
      tagsList: {
        title: 'Technologies',
        tags: [
          typescript(),
          mongoDb(),
          nodejs(),
          react(),
          pnpm(),
          jest(),
          prettier(),
          antd(),
          express(),
          expressGateway(),
          git(),
          docker(),
          html5(),
          css3(),
          sass(),
        ],
      },
      links: [demo({ url: 'http://desktop.kiosv.store' })],
      screenshots: [
        { src: import('@/assets/portfolio/1.png'), alt: 'First screenshot' },
        { src: import('@/assets/portfolio/2.png'), alt: 'Second screenshot' },
        { src: import('@/assets/portfolio/3.png'), alt: 'Third screenshot' },
        { src: import('@/assets/portfolio/4.png'), alt: 'First screenshot' },
        { src: import('@/assets/portfolio/5.png'), alt: 'Second screenshot' },
        { src: import('@/assets/portfolio/6.png'), alt: 'Third screenshot' },
        { src: import('@/assets/portfolio/7.png'), alt: 'First screenshot' },
        { src: import('@/assets/portfolio/8.png'), alt: 'Second screenshot' },
        { src: import('@/assets/portfolio/9.png'), alt: 'Third screenshot' },
        { src: import('@/assets/portfolio/10.png'), alt: 'Third screenshot' },
      ],
    },
    {
      name: 'GE Business',
      image: import('@/assets/logos/galaxy.png'),
      dates: [new Date('2020-03'), null],
      details: [
        { label: 'Team size', value: '7 persons' },
        { label: 'My role', value: ['Front-end Developer'] },
        { label: 'Company', value: 'Galaxy Education JSC' },
        { label: 'Category', value: ['Web app', 'Product'] },
      ],

      description:
        'Develop management system for sales, marketing, and operations teams. Build order management, catalog management, promotion and transaction management. Build a system to manage the entire process of the company.',
      tagsList: {
        title: 'Technologies',
        tags: [react(), sass(), pnpm(), eslint(), prettier(), antd()],
      },
      links: [],
    },
    {
      name: 'ICAN ID',
      image: import('@/assets/logos/ican.png'),
      dates: [new Date('2019-06'), new Date('2020-02')],
      details: [
        { label: 'Team size', value: '3 peoples' },
        { label: 'My role', value: ['Front-end Developer'] },
        { label: 'Company', value: 'Galaxy Education JSC' },
        { label: 'Category', value: ['Web app', 'Product'] },
      ],
      // pdfDetails: [
      //   { label: 'Demo', value: 'https://tru-quest-ck7ea3.netlify.app', url: '#' },
      //   { label: 'Repository', value: 'https://github.com/mark-freeman/tru-quest', url: '#' },
      // ],
      description:
        'User identification project to support third parties to integrate pre-built systems such as login, registration, information modification, password change, user verification, forgot password, etc.',
      tagsList: {
        title: 'Technologies',
        tags: [react(), tailwindCss(), nextJs(), typescript(), pnpm()],
      },
      links: [],
    },
    {
      name: 'ICAN PAYMENT',
      image: import('@/assets/logos/ican.png'),
      dates: [new Date('2018-01'), new Date('2020-12')],
      details: [
        { label: 'Team size', value: '4 peoples' },
        { label: 'My role', value: ['Front-end Developer'] },
        { label: 'Company', value: 'None' },
        { label: 'Category', value: ['Web app', 'Product'] },
      ],
      // pdfDetails: [
      //   { label: 'Demo', value: '', url: '#' },
      //   { label: 'Repository', value: '', url: '#' },
      // ],
      description:
        'The project aims to develop a payment system that integrates popular payment gateways into a comprehensive payment solution for third-party integration, optimizing payment pages for sellers, accountants, and operators.',
      tagsList: {
        title: 'Technologies',
        tags: [react(), typescript(), nextJs(), pnpm(), tailwindCss(), typescript()],
      },
      links: [],
    },
  ],
} as const satisfies ReadonlyDeep<PortfolioSection>;

export default portfolioSectionData;
