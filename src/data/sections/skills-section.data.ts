import type { SkillsSection } from '@/types/sections/skills-section.types';
import type { ReadonlyDeep } from 'type-fest';
import {
  antd,
  apolloGraphql,
  astro,
  axios,
  bootstrap,
  chakraUi,
  css3,
  cypress,
  eslint,
  express,
  firebase,
  git,
  html5,
  javascript,
  jquery,
  mongoDb,
  nestJs,
  nextJS,
  nuxtJS,
  pnpm,
  postgreSql,
  prettier,
  react,
  redux,
  sass,
  strapi,
  supabase,
  tailwindCss,
  typescript,
  vscode,
  vue,
  webstorm,
  yarn,
} from '../helpers/skills';
import { cs } from 'date-fns/locale';

const skillsSectionData = {
  config: {
    title: 'Skills',
    slug: 'skills',
    icon: 'fa6-solid:bars-progress',
    visible: true,
  },
  skillSets: [
    {
      title: 'I already know',
      skills: [
        react({
          level: 4,
        }),
        redux({ level: 4 }),
        vue({
          level: 4,
        }),
        nuxtJS({ level: 4 }),
        nextJS({ level: 4 }),
        antd({ level: 4 }),
        typescript({
          level: 4,
        }),
        sass({
          level: 4,
        }),
        tailwindCss({ level: 4 }),
        prettier({ level: 4 }),
        eslint({
          level: 4,
        }),
        mongoDb({ level: 3 }),
        pnpm({ level: 3 }),
        express({ level: 3 }),
        typescript({ level: 3 }),
        bootstrap({ level: 4 }),
        git({ level: 3 }),
        yarn({ level: 3 }),
        html5({ level: 4 }),
        css3({ level: 4 }),
        javascript({ level: 4 }),
        jquery({ level: 4 }),
        vscode({ level: 4 }),
        strapi({ level: 4 }),
      ],
    },
    // {
    //   title: 'I want to learn',
    //   skills: [apolloGraphql(), astro(), supabase(), cypress()],
    // },
    {
      title: 'Languages',
      skills: [
        { icon: 'circle-flags:vn', name: 'Vietnamese' },
        { icon: 'circle-flags:us', name: 'English' },
      ],
    },
  ],
} as const satisfies ReadonlyDeep<SkillsSection>;

export default skillsSectionData;
