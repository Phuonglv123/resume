import type { MainSection } from '@/types/sections/main-section.types';
import type { ReadonlyDeep } from 'type-fest';
import { facebook, github, linkedin, twitter } from '../helpers/links';

const mainSectionData = {
  config: {
    icon: 'fa6-solid:user',
    title: 'Profile',
    slug: 'profile',
    visible: true,
  },
  image: import('@/assets/me.jpeg'),
  fullName: 'Le Van Phuong',
  role: 'Frontend Developer',
  details: [
    { label: 'Phone', value: '055 979 6913', url: 'tel:0559796913' },
    { label: 'Email', value: 'vanphuong479@gmail.com', url: 'mailto:vanphuong479@gmail.com' },
    { label: 'Address', value: 'Le Van Chi Street, Thu Duc Ward, Thu Duc City, HCM City' },
  ],
  pdfDetails: [
    { label: 'Phone', value: '055 979 6913' },
    { label: 'Email', value: 'vanphuong479@gmail.com' },
    { label: 'LinkedIn', value: '/in/le-phuong-02271a18a/', url: 'https://www.linkedin.com' },
    { label: 'Website', value: 'mark-freeman-personal-website.com', url: '/', fullRow: true },
  ],
  description:
    'I am an experienced Frontend developer with over 3 years of experience in designing and developing user-friendly web applications. My passion for technology and creativity drives me to come up with intuitive and innovative web solutions. I am a fast learner and always eager to keep up with the latest web development trends.',
  tags: [{ name: 'Open to work' }, { name: 'Available for mentoring' }, { name: 'Working on side project' }],
  action: {
    label: 'Download CV',
    url: '/cv.pdf',
    downloadedFileName: 'CV-PhuongLV.pdf',
  },
  links: [linkedin({ url: 'https://www.linkedin.com/in/le-phuong-02271a18a/' })],
} as const satisfies ReadonlyDeep<MainSection>;

export default mainSectionData;
